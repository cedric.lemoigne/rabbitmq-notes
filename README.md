# RabbitMQ and Nodejs

* RabbitMQ: https://dev.to/usamaashraf/microservices--rabbitmq-on-docker-e2f
* https://hub.docker.com/\_/rabbitmq
* https://www.sysrun.io/2015/11/02/dockerize-rabbitmq/

## docker-compose

When the container is ran a web interface to administrate the broker can be found
 at 127.0.0.1:15762. The default username and password are guest/guest.

By default RabbitMQ uses the port 5672.

## RabbitMQ tutorial
* https://www.rabbitmq.com/mqtt.html
* https://www.rabbitmq.com/tutorials/tutorial-one-javascript.html

see official-tutorial directory


## microservices with RabbitMQ
* https://www.sipmann.com/microservices_nodejs_express_rabbitmq_part_1.html#.XJUBCqeYWV4

see microservices-with-rabbimq directory
