#!/usr/bin/env node

const amqp = require('amqplib/callback_api')
amqp.connect('amqp://localhost', function (_err, conn) {
  conn.createChannel(function (_err, ch) {
    const q = 'hello'

    ch.assertQueue(q, { durable: false })
    ch.sendToQueue(q, Buffer.from('Hello World!'))
    console.log(' [x] sent Hello World!')
  })
  setTimeout(() => {
    conn.close()
    process.exit(0)
  }, 500)
})
