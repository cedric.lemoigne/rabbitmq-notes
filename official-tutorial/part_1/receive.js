#!/usr/bin/env node

const amqp = require('amqplib/callback_api')

amqp.connect('amqp://localhost', function (_err, conn) {
  conn.createChannel(function (_err, ch) {
    const q = 'hello'
    ch.assertQueue(q, { durable: false })
    console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', q)
    ch.consume(q, function (msg) {
      console.log(' [x] Received %s', msg.content.toString())
    }, { noAck: true })
  })
})
