#!/usr/bin/env node

const amqp = require('amqplib/callback_api')

const msg = process.argv.slice(2).join(' ') || 'Hello World!'

amqp.connect('amqp://localhost', function (_err, conn) {
  conn.createChannel(function (_err, ch) {
    const q = 'saucisse'

    ch.assertQueue(q, { durable: true })
    ch.sendToQueue(q, Buffer.from(msg), { persistent: true })
    console.log(' [x] sent %s', msg)
  })
  setTimeout(() => {
    conn.close()
    process.exit(0)
  }, 500)
})
